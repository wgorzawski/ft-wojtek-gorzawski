import Vue from 'vue'
import VueRouter from 'vue-router'
import UserList from '../views/UserList'
import UserModyfiy from '../views/UserModyfiy'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'AddUser',
    component: UserModyfiy
  },
  {
    path: '/user/edit/:id',
    name: 'EditUser',
    component: UserModyfiy
  },
  {
    path: '/user-list/:id?',
    name: 'UserList',
    component: UserList
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
