import axios from 'axios'

export default {
  getUsers (id) {
    if (id) {
      return axios.get(`/users?page=${id}`).then((response) => {
        return response.data
      })
    }
    return axios.get('/users').then((response) => {
      return response.data
    })
  },

  getUser (id) {
    return axios.get(`/users/${id}`).then((response) => {
      return response.data
    })
  },

  deleteUser (id) {
    return axios.delete(`/users/${id}`).then((response) => {
      return response.data
    })
  },

  createUser (user) {
    return axios.post('/users/', user).then((response) => {
      return response.data
    })
  },

  updateUser (user) {
    return axios.put(`/users/${user.id}`, user).then((response) => {
      return response.data
    })
  }
}
