import Vue from 'vue'
import Vuex from 'vuex'
import { SET_TITLE } from './mutations'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    title: null
  },
  getters: {
    title: (state) => state.title
  },
  actions: {
    setTitle ({ commit }, title) {
      commit(SET_TITLE, title)
    }
  },
  mutations: {
    [SET_TITLE] (state, title) {
      state.title = title
    }
  },
  modules: {
  }
})
